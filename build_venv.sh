#!/bin/sh
set -e

mkdir -p /opt/pyvenv

rm -rf /opt/pyvenv/$1
python3 -m venv /opt/pyvenv/$1
cd /opt/pyvenv/$1
. bin/activate
pip install -r $2

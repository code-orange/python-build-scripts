#!/bin/bash

# VARIABLES
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
echo $SCRIPTPATH

cd $SCRIPTPATH/../../

if [ -d "pkg/filesystem/" ]; then
  cp --no-target-directory -r pkg/filesystem/ /
  systemd-tmpfiles --create
fi

python3 -m venv venv
. venv/bin/activate

pip install --upgrade pip
pip install --upgrade wheel

pip install --no-binary lxml -r requirements.txt

if [ -d "pkg/filesystem/etc/systemd/system" ]; then
  unit_files=`ls pkg/filesystem/etc/systemd/system`
  systemctl daemon-reload
  systemctl enable $unit_files --now
  systemctl restart $unit_files
fi
